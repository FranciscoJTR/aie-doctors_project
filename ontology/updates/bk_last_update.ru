PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xmlns: <http://www.semanticweb.org/kiko/ontologies/2018/10/untitled-ontology-2#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clinical-ontology: <http://www.semanticweb.org/kiko/ontologies/2018/10/clinical-ontology#>

    INSERT DATA {
clinical-ontology:Uso_de_drogas clinical-ontology:pode_provocar clinical-ontology:Osteomielite .
clinical-ontology:Infecção clinical-ontology:pode_provocar clinical-ontology:Osteomielite .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:Abscesso .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:Infecção .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:Infecção .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:Dor .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:Infecção .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:Febre .
clinical-ontology:Tc clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:Tc clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:Tc clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:Tc clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:Rm clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:Demência clinical-ontology:pode_provocar clinical-ontology:Pneumonia .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:Taquipneia .
clinical-ontology:Palpação clinical-ontology:suporta clinical-ontology:Pneumonia .
clinical-ontology:Salmonelose clinical-ontology:manifesta clinical-ontology:Dor .
clinical-ontology:Salmonelose clinical-ontology:manifesta clinical-ontology:Febre .
clinical-ontology:Salmonelose clinical-ontology:manifesta clinical-ontology:Cefaleia .
clinical-ontology:Salmonelose clinical-ontology:manifesta clinical-ontology:Anorexia .
clinical-ontology:Tétano clinical-ontology:manifesta clinical-ontology:Trismo .
}