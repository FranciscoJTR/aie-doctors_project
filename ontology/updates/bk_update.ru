PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xmlns: <http://www.semanticweb.org/kiko/ontologies/2018/10/untitled-ontology-2#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX clinical-ontology: <http://www.semanticweb.org/kiko/ontologies/2018/10/clinical-ontology#>

INSERT DATA {
  owl:a_intervenção_profilática_mais_importante clinical-ontology:é_evitar_a_intubação_de owl:risco_para_PAV .
  owl:a_intervenção_profilática_mais_importante2 clinical-ontology:é_evitar_a_intubação_de2 owl:risco_para_PAV2 .
  owl:a_intervenção_profilática_mais_importante3 clinical-ontology:é_evitar_a_intubação_de3 owl:risco_para_PAV3 .
}
