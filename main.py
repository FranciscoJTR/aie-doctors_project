import subprocess as sp
import nltk
import nltk.data
from nltk.tag.stanford import StanfordNERTagger
import re
import regex

PDF_DIR = "docs/pdf/"
RAW_TXT_DIR = "docs/txt/raw/"
CLEAN_TXT_DIR = "docs/txt/clean/"
NER_JAR = "./ner_tagger/stanford-ner-3.9.2.jar"
NER_MODEL = "./ner_tagger/ner-model-portuguese.ser.gz"
CLINICAL_ONTOLOGY = "clinical-ontology:"
SYMPTOM = "SYMPTOM"
EXAM = "EXAM"
RISK_FACTOR = "RISK_FACTOR"

def get_files_from(directory):
    return (sp
            .check_output(["ls", "-1", directory])
            .decode("utf-8")
            .split("\n"))[:-1]

def extract_filename(filename):
    return filename.split(".")[0]

def to_text(pdf):
    txt = extract_filename(pdf) + ".txt"
    sp.call(["pdftotext", "-layout", "-nopgbrk", PDF_DIR + pdf, RAW_TXT_DIR + txt])

def convert_pdfs_to_text(pdfs_list):
    for pdf in pdfs_list:
        to_text(pdf)

def write_string_as_file(string, filename):
    open(filename, "w").write(string)

def delete_pattern(pattern, strings_list):
    return list(map(lambda text: regex.sub(pattern, "", text, flags=re.M|re.DOTALL), strings_list))

def load_files_as_strings(directory, files):
    files_as_strings = []
    
    for file in files:
        string = get_file_as_string(directory + file)
        files_as_strings.append(string)

    return files_as_strings

def pre_process(txts_list):
    files_as_strings = load_files_as_strings(RAW_TXT_DIR, txts_list)
    # DELETE TABLES
    without_tables = delete_pattern("QUADRO .+?^$.+?(^$\n){2}", files_as_strings)
    # DELETE TABLE REFERENCES
    without_references = delete_pattern("\s\((Quadro|Fig\.)\s[0-9]+(\.[0-9]+)?\)", without_tables)
    # DELETE TITLES
    without_titles = delete_pattern("^[ ]*\p{Lu}[\p{Lu}:\d ]+$\n", without_references)
    # DELETE SECTIONS AND NAMES
    without_sections_and_names = delete_pattern("^\d+\n[\p{L} ]+\n\p{L}+ (\p{Lu}\. )?\p{L}+(, \p{L}+ \p{Lu}\. \p{L}+)*$\n", without_titles)
    list(map(lambda string, name: write_string_as_file(string, CLEAN_TXT_DIR + name), without_sections_and_names, txts_list))

    return get_files_from(CLEAN_TXT_DIR)

def get_file_as_string(filename):
    return open(filename, "r").read()

def detect_sentences(txts_list):
    sentence_detector = nltk.data.load("tokenizers/punkt/portuguese.pickle")

    texts = []
    for txt in txts_list:
        text = get_file_as_string(CLEAN_TXT_DIR + txt)
        texts.append("\n\n".join(sentence_detector.tokenize(text.strip())))

    return texts

def pos_tag(texts):
    tagged_texts = []

    for text in texts:
        echo_args = ["echo", text]
        tree_tagger_args = ["tree_tagger/cmd/tree-tagger-portuguese"]
        echo = sp.Popen(echo_args, stdout=sp.PIPE)
        tree_tagger = sp.Popen(tree_tagger_args, stdin=echo.stdout, stdout=sp.PIPE, stderr=sp.PIPE)
        tagged_words = tree_tagger.communicate()[0].decode("utf-8").split("\n")[:-1]
        tagged_texts.append([tuple(tw.split("\t")) for tw in tagged_words])

    return tagged_texts

def load_ner_list():
    named_entities = {}

    for line in open("aux_lists/ner_list.csv", "r"):
        entity, ent_type = line.split(",")
        
        if entity not in named_entities:
            named_entities[entity] = ent_type.strip()

    return named_entities

def ner_tag(pos_tagged_texts):
    named_entities = load_ner_list()
    ner_pos_tagged = []

    for tagged_text in pos_tagged_texts:
        ner_tagged_text = []

        for tag_tuple in tagged_text:
            new_tag_tuple = tag_tuple
            ent_type = named_entities.get(tag_tuple[0])

            if ent_type is not None:
                new_tag_tuple += (ent_type,)

            ner_tagged_text.append(new_tag_tuple)

        ner_pos_tagged.append(ner_tagged_text)

    return ner_pos_tagged

def rel_ext():
    relations = {}

    for txt in get_files_from(CLEAN_TXT_DIR):
        relations[txt.split(".")[0]] = (list(map(
            lambda line: line.split("\t"), 
            (sp
                .check_output(["Linguakit-master/linguakit", "rel", "pt", CLEAN_TXT_DIR + txt])
                .decode("utf-8")
                .split("\n")[:-1]))))

    return relations

def replace_chars(string):
    string = regex.sub("[ @,/%\.°–“]", "_", string)
    string = regex.sub("≤", "leq", string)
    string = regex.sub("≥", "geq", string)
    return string

def write_relations_to_file(text_rels):
    update_string = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xmlns: <http://www.semanticweb.org/kiko/ontologies/2018/10/untitled-ontology-2#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clinical-ontology: <http://www.semanticweb.org/kiko/ontologies/2018/10/clinical-ontology#>

    INSERT DATA {\n"""

    for rels in text_rels:

        for rel in rels:
            update_string += " ".join(rel) + " .\n"

    update_string += "}"
    open("ontology/updates/update.ru", "w").write(update_string)

def check_for(type, type_dict, string):
    return list(filter(lambda ent: ent in string, type_dict[type]))

def check_for_risk_factors(disease, rels, type_dict):
    res = []

    for r in rels:
        if(("fatores de risco" in r[1]) and (regex.search("(são|incluem)", r[2]) is not None)):
            ent_list = check_for(RISK_FACTOR, type_dict, replace_chars(r[3]))

            for ent in ent_list:
                res.append(
                    (CLINICAL_ONTOLOGY + ent.capitalize(),
                    CLINICAL_ONTOLOGY + "pode_provocar", 
                    CLINICAL_ONTOLOGY + disease.capitalize()
                    )
                )

    return res

def check_for_symptoms(disease, rels, type_dict):
    res = []

    for r in rels:
        if(regex.search("(sinal|sinais|sintoma|apresenta|manifesta)", r[1]) is not None
            or
            regex.search("(sinal|sinais|sintoma|apresenta|manifesta)", r[2]) is not None
            ):
            ent_list = check_for(SYMPTOM, type_dict, replace_chars(r[3]))

            for ent in ent_list:
                res.append(
                    (CLINICAL_ONTOLOGY + disease.capitalize(),
                    CLINICAL_ONTOLOGY + "manifesta",
                    CLINICAL_ONTOLOGY + ent.capitalize()
                    )
                )

    return res

def check_for_exams(disease, rels, type_dict):
    res = []

    for r in rels:
        if(regex.search("(revel|detect)", r[2]) is not None):
            ent_list = check_for(EXAM, type_dict, replace_chars(r[1]))

            for ent in ent_list:
                res.append(
                    (CLINICAL_ONTOLOGY + ent.capitalize(),
                    CLINICAL_ONTOLOGY + "suporta",
                    CLINICAL_ONTOLOGY + disease.capitalize()
                    )
                )

    return res

def load_ents_by_type():
    type_dict = {}

    for line in open("aux_lists/ner_list.csv", "r"):
        entity, ent_type = line.split(",")
        ent_type = ent_type.strip()
        
        if ent_type not in type_dict:
            type_dict[ent_type] = [entity]
        else:
            type_dict[ent_type].append(entity)

    return type_dict


def filter_relations(text_rels):
    type_dict = load_ents_by_type()
    filtered_rels = []

    for disease,rels in text_rels.items():
        non_empty_rels = list(filter(lambda x: "" not in x, rels))
        filtered_rels.append(check_for_risk_factors(disease, non_empty_rels, type_dict))
        filtered_rels.append(check_for_symptoms(disease, non_empty_rels, type_dict))
        filtered_rels.append(check_for_exams(disease, non_empty_rels, type_dict))

    return filtered_rels

def upload_rels(text_rels):
    write_relations_to_file(text_rels)

    UPDATE_CMD = "ontology/apache-jena-fuseki-3.9.0/bin/s-update"
    UPDATE_ENDPNT = "http://localhost:3030/clinical/update"
    UPDATE_FILE = "--file=ontology/updates/update.ru"

    update_args = [UPDATE_CMD, "--service", UPDATE_ENDPNT, UPDATE_FILE]
    sp.call(update_args)


if __name__ == "__main__":
    pdfs = get_files_from(PDF_DIR)
    convert_pdfs_to_text(pdfs)
    raw_txts = get_files_from(RAW_TXT_DIR)
    clean_txts = pre_process(raw_txts)
    texts = detect_sentences(clean_txts)
    pos_tagged_texts = pos_tag(texts)
    ner_pos_tagged_texts = ner_tag(pos_tagged_texts)
    relations = rel_ext()
    filtered_relations = filter_relations(relations)
    upload_rels(filtered_relations)
    

