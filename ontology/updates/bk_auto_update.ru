PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xmlns: <http://www.semanticweb.org/kiko/ontologies/2018/10/untitled-ontology-2#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clinical-ontology: <http://www.semanticweb.org/kiko/ontologies/2018/10/clinical-ontology#>

    INSERT DATA {
clinical-ontology:o_uso_de_drogas clinical-ontology:pode_provocar clinical-ontology:Osteomielite .
clinical-ontology:infecção_de_o_pé_diabético clinical-ontology:pode_provocar clinical-ontology:Osteomielite .
clinical-ontology:demência clinical-ontology:pode_provocar clinical-ontology:Pneumonia .
clinical-ontology:pneumonia_pneumocócica clinical-ontology:pode_provocar clinical-ontology:Pneumonia .
}