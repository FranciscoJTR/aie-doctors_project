PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX xmlns: <http://www.semanticweb.org/kiko/ontologies/2018/10/untitled-ontology-2#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clinical-ontology: <http://www.semanticweb.org/kiko/ontologies/2018/10/clinical-ontology#>

    INSERT DATA {
clinical-ontology:o_uso_de_drogas clinical-ontology:pode_provocar clinical-ontology:Osteomielite .
clinical-ontology:infecção_de_o_pé_diabético clinical-ontology:pode_provocar clinical-ontology:Osteomielite .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:crianças_e_adultos .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:inespecíficos .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:abscessos_espinais .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:como_uma_infecção .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:um .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:inespecíficos .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:aguda .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:como_sinais_de_infecção .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:o .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:dor_persistente .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:sinais_locais_de_infecção .
clinical-ontology:Osteomielite clinical-ontology:manifesta clinical-ontology:como_febre .
clinical-ontology:A_TC clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:A_TC clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:A_TC clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:A_TC_e_a_RM clinical-ontology:suporta clinical-ontology:Osteomielite .
clinical-ontology:demência clinical-ontology:pode_provocar clinical-ontology:Pneumonia .
clinical-ontology:pneumonia_pneumocócica clinical-ontology:pode_provocar clinical-ontology:Pneumonia .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:indolente_ou_fulminante .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:achados .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:os_tipos_II_ou_III .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:os_hospitais .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:7 .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:as_mesmas_de_os .
clinical-ontology:Pneumonia clinical-ontology:manifesta clinical-ontology:taquipneia .
clinical-ontology:A_palpação clinical-ontology:suporta clinical-ontology:Pneumonia .
clinical-ontology:pneumophila clinical-ontology:suporta clinical-ontology:Pneumonia .
}